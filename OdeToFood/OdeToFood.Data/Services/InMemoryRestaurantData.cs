﻿using System;
using System.Collections.Generic;
using System.Linq;
using OdeToFood.Data.Models;

namespace OdeToFood.Data.Services
{
    public class InMemoryRestaurantData : IRestaurantData
    {
        private List<Restaurant> restaurants;

        public InMemoryRestaurantData()
        {
            restaurants = new List<Restaurant>()
            {
                new Restaurant{ Id = 1, Name = "Dobos Pub", Cuisine = CuisineType.Hungarian},
                new Restaurant{ Id = 2, Name = "Purple Panda", Cuisine = CuisineType.Asian},
                new Restaurant{ Id = 3, Name = "Forte Pizza", Cuisine = CuisineType.Italian},
                new Restaurant{ Id = 3, Name = "Hellas", Cuisine = CuisineType.Greek},
                new Restaurant{ Id = 3, Name = "KFC", Cuisine = CuisineType.American}
            };
        }
        public IEnumerable<Restaurant> GetAll()
        {
            return restaurants.OrderBy(restaurant => restaurant.Name);
        }
    }
}